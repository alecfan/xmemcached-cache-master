package org.apache.ibatis.caches.memcached;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;

import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.utils.AddrUtil;

/**
 * 
 *
 * @author chenld(liangdengchen@cyou-inc.com)
 * @date 2013-8-19
 */
public class MybatisXMemcachedClientBuilder extends XMemcachedClientBuilder{
    
    /**
     * key prefix
     */
    private String keyPrefix = "mybatis-xmemcached-";
    
    /**
     * unit:seconds
     */
    private int expiration;
    
    public MybatisXMemcachedClientBuilder(String addressList) {
        this(AddrUtil.getAddresses(addressList));
    }

    public MybatisXMemcachedClientBuilder(List<InetSocketAddress> addressList) {
        super(addressList);
    }

    public MybatisXMemcachedClientBuilder(List<InetSocketAddress> addressList,
            int[] weights) {
        super(addressList, weights);
    }

    public MybatisXMemcachedClientBuilder() {
        super((Map<InetSocketAddress, InetSocketAddress>) null);
    }

    
    public String getKeyPrefix() {
        return keyPrefix;
    }

    
    public void setKeyPrefix(String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    
    public int getExpiration() {
        return expiration;
    }

    
    public void setExpiration(int expiration) {
        this.expiration = expiration;
    }     
    
    
}
